import T101 from '~/components/Topics/Components/tasks/T101';
import T102 from '~/components/Topics/Components/tasks/T102';
import T103 from '~/components/Topics/Components/tasks/T103';
import T104 from '~/components/Topics/Components/tasks/T104';
import T201 from '~/components/Topics/Components/tasks/T201';
import T203 from '~/components/Topics/Components/tasks/T203';
import T202 from '~/components/Topics/Components/tasks/T202';
import T301 from "~/components/Topics/Components/tasks/T301";
import T307 from "~/components/Topics/Components/tasks/T307";
import T302 from "~/components/Topics/Components/tasks/T302";
import T303 from "~/components/Topics/Components/tasks/T303";
import T304 from "~/components/Topics/Components/tasks/T304";
import T305 from "~/components/Topics/Components/tasks/T305";
import T306 from "~/components/Topics/Components/tasks/T306";

const TOPICS_TYPES = {
  T101: 'T101',
  T102: 'T102',
  T103: 'T103',
  T104: 'T104',
  T201: 'T201',
  T202: 'T202',
  T203: 'T203',
  T301: 'T301',
  T302: 'T302',
  T303: 'T303',
  T304: 'T304',
  T305: 'T305',
  T306: 'T306',
  T307: 'T307',
  T401: 'T401',
  T402: 'T402',
  T403: 'T403',
  T404: 'T404',
  T405: 'T405',
  T406: 'T406',
  T407: 'T407',
  T408: 'T408',
  T409: 'T409',
  T410: 'T410',
};

export default {
  [TOPICS_TYPES.T101]: {
    component: T101
  },
  [TOPICS_TYPES.T102]: {
    component: T102
  },
  [TOPICS_TYPES.T103]: {
    component: T103
  },
  [TOPICS_TYPES.T104]: {
    component: T104
  },
  [TOPICS_TYPES.T201]: {
    component: T201
  },
  [TOPICS_TYPES.T202]: {
    component: T202
  },
  [TOPICS_TYPES.T203]: {
    component: T203
  },
  [TOPICS_TYPES.T301]: {
    component: T301
  },
  [TOPICS_TYPES.T302]: {
    component: T302
  },
  [TOPICS_TYPES.T303]: {
    component: T303
  },
  [TOPICS_TYPES.T304]: {
    component: T304
  },
  [TOPICS_TYPES.T305]: {
    component: T305
  },
  [TOPICS_TYPES.T306]: {
    component: T306
  },
  [TOPICS_TYPES.T307]: {
    component: T307
  },
};
