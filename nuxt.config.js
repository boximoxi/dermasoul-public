export default {
	/** disable nuxt telemetry */
	telemetry: false,

	/** deployment target for nuxt */
	target: 'static',

	head: {
		title: 'DermaSoul',

		htmlAttrs: {
			lang: 'hu',
		},

		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: 'DermaSoul' },
			{ name: 'format-detection', content: 'telephone=no' },
      { name: 'msapplication-TileColor', content: '#da532c' },
      { name: 'theme-color', content: '#ffffff' },
		],

		link: [
      { rel: 'icon', type: 'image/x-icon', href: '/fav/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/fav/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/fav/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/fav/favicon-16x16.png' },
      { rel: 'mask-icon', href: '/fav/site.webmanifest' }
    ]

  },

	css: [
    'hooper/dist/hooper.css',
    '~/assets/fonts.css',
    '~/assets/scss/main.scss'
  ],

	plugins: [
    '~/static/js/bootstrap.bundle.min.js'
  ],

	/** auto import components when used in templates */
	components: true,

	buildModules: ['@nuxt/postcss8'],

	modules: [
    '@nuxtjs/style-resources',
    '~/modules/router'
  ],

  styleResources: {
    scss: ['./assets/scss/*.scss']
  },

	build: {
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.(pdf|ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      })
    }
	},

  ssr: false
};
