# Dermasoul 

### Installation

```bash
# install dependencies
$ yarn install
```

### Build Setup
```bash
# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

### Includes

- Vue 2
- Nuxt 2
- Bootstrap 5 (SCSS, JS)
- headroom.js
- Hooper
