export default function () {
  this.extendRoutes((routes, resolve) => {
    routes.push({
      name: 'Impresszum',
      path: '/impresszum',
      props: {
        type: 'staticPage'
      },
      component: resolve(__dirname, '@/components/StaticContents/Impresszum')
    });

    routes.push({
      name: 'Adatkezelési tájékoztató',
      path: '/adatkezelesi-tajekoztato',
      props: {
        type: 'staticPage'
      },
      component: resolve(__dirname, '@/components/StaticContents/Adatkezelesi')
    });

    routes.push({
      name: 'Általános szerződési feltételek',
      path: '/altalasnos-szerzodesi-feltetelek',
      props: {
        type: 'staticPage'
      },
      component: resolve(__dirname, '@/components/StaticContents/ASZF')
    });
  });
}
