export const state = () => ({
  list: []
})

export const mutations = {
  add(state, { name, topicId, icon, title, hash }) {
    state.list.push({
      name,
      topicId,
      icon,
      title,
      hash
    });
  },
  remove(state, name) {
    return state.list = state.list.filter(item => item.name !== name);
  }
}
