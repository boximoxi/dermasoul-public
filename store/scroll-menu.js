export const state = () => ({
  list: [],
  headerMenu: null
})

export const mutations = {
  add(state, list) {
    state.list = list;
  },
  remove(state, name) {
    return state.list = state.list.filter(item => item.name !== name);
  },
  toggleMenu(state, toggle) {
    state.headerMenu = toggle;
  }
}
